control_db is a control database made of random protein + protein taken at random at uniprot.

* big_clusters.fa
fasta format, 19029 proteins that are in the biggest clusters

* big clusters_aligned.fa
mafft alignment of the biggest clusters. shape = (19029, 2718)

* biggest_clusters_aligned_removed_gaps.json
proteins of biggest clusters aligned, with gap position removed (filter 0.9). shape 19029*581


* biggest_clusters_aligned_removed_gaps.csv
proteins of biggest clusters aligned, with secondary structure