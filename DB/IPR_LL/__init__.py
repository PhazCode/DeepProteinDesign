import os
import sys
import time
import json
import numpy as np
import pandas as pd
from tqdm import tqdm
from collections import Counter
import matplotlib.pyplot as plt

plt.style.use('seaborn-white')

db = pd.read_csv('DB/IPR_LL/db.csv', index_col=0)
# cca = pd.read_csv('DB/IPR_LL/clusters_compressed_alignments.csv')
clusters_data = pd.read_csv('DB/IPR_LL/clusters_data.csv')
