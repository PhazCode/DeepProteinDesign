Documentation for the organization of the database
Each folder represents a protein family.

LL:
Luciferase Like Domain (IPR011251)
The identifiers are UniprotKB identifiers
Downloaded here https://www.ebi.ac.uk/interpro/entry/IPR011251/proteins-matched
DL date : 13 April 2018
119611 proteins at this date
Dropping proteins with length>500 and length<250 and not starting by methioneine.
111936 proteins left

IPR017853:
Cellulases from Interpro.
It is called the Glycoside hydrolase superfamily.
The identifiers are UniprotKB identifiers
Downloaded here : http://www.ebi.ac.uk/interpro/entry/IPR017853/proteins-matched
DL date : 13 April 2018
583011 proteins at this date.


WW:
WW domain (PF00397)
Downloaded at http://pfam.xfam.org/family/pf00397#tabview=tab3 the 23 April 2018.
17233 proteins at this date.
