import sys

sys.path.append('/Volumes/@home/DeepSeqGen/')

import numpy as np
import pandas as pd
from bokeh.io import curdoc, output_file, show
from bokeh.layouts import row, widgetbox, column
from bokeh.models.widgets import Slider, TextInput, Button, Select
from bokeh.plotting import figure
from utils.encoding import aa_letters, aa_ids
from utils import luxa_name, cond_names
from generative_models import ProtCNN
from utils import sequence, encoding

model = ProtCNN().load_weights('generative_models/weights/sampling_std/0.1.h5')

luxa = pd.read_csv('DB/IPR_LL/luxa.csv').iloc[0]
luxa_oh = encoding.to_one_hot(encoding.pad([luxa.seq], model.original_dim))
luxa_conds = np.array(luxa[cond_names]).reshape(1, 6)
luxa_mean, luxa_var = model.E.predict([luxa_oh, luxa_conds])  # latent space [z_mean, z_var] # no sampling
luxa_sample = luxa_mean


def mismatches(seq_a, seq_b):
    seq_b = seq_b[:len(seq_a)]
    seq_b = seq_b + max(0, len(seq_a) - len(seq_b)) * '-'
    return [int(a != b) for (a, b) in zip(seq_b, seq_a)]


class MismatchesControlPlot:
    def __init__(self, variant_function, title='LuxA variants mismatches visualization'):
        self.forced_aa = model.original_dim * [None]
        self.forced_aa_history = []
        self.plot_history = []
        self.variant = None
        self.variant_function = variant_function
        plot = figure(x_range=(0, len(luxa.seq)), y_range=(0, 2), height=200, width=900,
                      x_axis_label='Sequence Position', title=title)
        plot.yaxis.ticker = [0.5, 1.5]
        plot.yaxis.major_label_overrides = {0.5: 'Secondary Struture', 1.5: 'Reconstructed Sequence'}

        self.img = plot.image(image=[np.zeros((2, 360))], x=0, y=0, dw=len(luxa.seq), dh=2,
                              palette=["#77BB77", "#BB7777"])

        # Set up widgets
        reset_button = Button(label='Reset')
        reset_button.on_click(self.reset)

        self.position = Slider(title="Sequence Position", value=0, start=0, end=len(luxa.seq) - 1, step=1)
        self.position.on_change('value', self.change_select)

        self.select = Select(title="Change Amino-Acid M to :", value="Random", options=['Random', *aa_letters])

        do_button = Button(label='Force')
        do_button.on_click(self.update_data)

        undo_button = Button(label='Undo')
        undo_button.on_click(self.undo)

        self.reset()

        wb = widgetbox(reset_button, self.position, self.select, do_button, undo_button)
        self.app = row(wb, plot)

    def change_select(self, attrname, old, new):
        self.select.title = 'Change Amino-Acid {0} to :'.format(self.variant[self.position.value])

    def reset(self):
        self.forced_aa = model.original_dim * [None]
        self.variant = self.variant_function(self.forced_aa)
        ss = sequence.ss(self.variant)
        self.plot_history = [[[mismatches(luxa.ss, ss), mismatches(luxa.seq, self.variant)]]]
        self.img.data_source.data['image'] = self.plot_history[0]

    def update_data(self):
        i, aa = self.position.value, self.select.value
        print('Changing Amino-Acid at position {0} to {1}'.format(i, aa))
        self.forced_aa_history.append(i)
        self.forced_aa[i] = aa
        self.variant = self.variant_function(self.forced_aa)
        print('Variant : ', self.variant)
        ss = sequence.ss(self.variant)
        print('Secondary Structure : ', ss)
        self.plot_history.append([[mismatches(luxa.ss, ss), mismatches(luxa.seq, self.variant)]])
        self.img.data_source.data['image'] = self.plot_history[-1]

    def undo(self):
        if len(self.plot_history) > 1:
            self.plot_history.pop()
            self.img.data_source.data['image'] = self.plot_history[-1]
            i = self.forced_aa_history.pop()
            self.forced_aa[i] = None


def protcnn_variant(forced_aa):
    print('Generating variant...')
    x = np.zeros((1, model.original_dim, 21))
    for i in range(model.original_dim):
        if forced_aa[i] is None:
            aa = np.argmax(model.G.predict([luxa_sample, luxa_conds, x])[0, i, :])
        elif forced_aa[i] == 'Random':
            aa = np.random.choice(21)
            print('Forcing {0} at position {1}'.format(aa_letters[aa], i))
        else:
            aa = aa_ids[forced_aa[i]]
            print('Forcing {0} at position {1}'.format(aa_letters[aa], i))
        x[0, i, aa] = 1
    return encoding.to_string(x, remove_gaps=True)[0]


def baseline_variant(forced_aa):
    return ''.join([(ls if aa is None else np.random.choice(aa_letters) if aa == 'Random' else aa) for (ls, aa) in
                    zip(luxa.seq, forced_aa)])


baseline_plot = MismatchesControlPlot(baseline_variant, title='BaseLine LuxA Variants Mismatches Visualisation')
protcnn_plot = MismatchesControlPlot(protcnn_variant, title='ProtCNN LuxA Variants Mismatches Visualisation')

curdoc().add_root(column(baseline_plot.app, protcnn_plot.app))
curdoc().title = "LuxA Variants Mismatches Visualization"
