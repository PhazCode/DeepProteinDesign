This is an interactive application to visualize how secondary structure changes
when sequence changes. To start application:

    bokeh --serve demo/luxapp
