from demo.lattice_proteins.utils import folding_score, cns_b62, cns, pairwise_dist_hist
from utils import ml, sequence, encoding
from utils.encoding import aa_letters
import numpy as np
from tqdm import tqdm
import pandas as pd
from generative_models import autoencoders as ae
from generative_models.simple_stats import MsaStats, BlosumStats
from bokeh.io import reset_output, output_file, show
from bokeh.plotting import figure
from bokeh.palettes import Spectral7

np.random.seed(0)
LP_STRUCTURE_ID = 624
intermediate_dim = 50
latent_dim = 10
dropout = 0

db_file = 'studies/E_lattice-proteins_old/MSA_A.csv'
seqlist_str = pd.read_csv(db_file).sequence[:46080]
weights = 1 / sequence.position_frequencies(seqlist_str)

## BENCHMARKING
N_SAMPLES = 100
n = len(seqlist_str)
p = np.random.permutation(len(seqlist_str))
ref_dataset, test_dataset = seqlist_str[p[100:]], seqlist_str[p[:100]]
var_dataset = np.random.choice(ref_dataset, N_SAMPLES, replace=None)
int_dataset = encoding.to_int(ref_dataset)

stddev = 3
model_10 = ae.BasicVAE(intermediate_dim=50, latent_dim=10, dropout=0)
model_10.compile(weights=weights)

model_10.load_weights('demo/lattice_proteins/weights/basicvae_MSA=A_i=50_l=10_d=0_rw.weights')
ae_variants_10 = [model_10.variant(s, 3) for s in var_dataset]

model_30 = ae.BasicVAE(intermediate_dim=200, latent_dim=30, dropout=0)
model_30.compile(weights=weights)
model_30.load_weights('demo/lattice_proteins/weights/basicvae_MSA=A_i=200_l=30_d=0_rw.weights')
ae_variants_30_1 = [model_30.variant(s, 3) for s in var_dataset]
ae_variants_30_2 = [model_30.variant(s, 25) for s in var_dataset]

# STATS VARIANTS
mutation_rate = 0.4
msa_stats = MsaStats(ref_dataset)
blosum_stats = BlosumStats()
stats_variants = [msa_stats.variant(seq, mutation_rate) for seq in var_dataset]
blosum_variants = [blosum_stats.variant(seq, mutation_rate) for seq in var_dataset]

cfg = pd.DataFrame(
    [dict(model_name='test', variants=test_dataset, color=Spectral7[1], label='Test Dataset'),
     dict(model_name='ae_10', variants=ae_variants_10, color=Spectral7[0], label='VAE 50-10 stddev 3'),
     dict(model_name='ae_30', variants=ae_variants_30_1, color=Spectral7[2], label='VAE 200-30 stdev 3'),
     dict(model_name='ae_30', variants=ae_variants_30_2, color=Spectral7[-3], label='VAE 200-30 stddev 25'),
     dict(model_name='msa_stats', variants=stats_variants, color=Spectral7[-2], label='Msa Stats Model'),
     dict(model_name='blosum_stats', variants=blosum_variants, color=Spectral7[-1], label='Blosum Stats Model'),
     dict(model_name="pca_model", variants=pca_variants, color='red', label="PCA Model")
     ])

reset_output()
output_file('demo/lattice_proteins/benchmark.html')

p = figure(title='Benchmarking', x_axis_label='Hamming Distance', width=800,
           y_axis_label='Folding Score for Structure #' + str(LP_STRUCTURE_ID))

for i, c in cfg.iterrows():
    x = np.array([cns(seq, int_dataset) for seq in encoding.to_int(c.variants)])
    y = np.array([folding_score(seq, LP_STRUCTURE_ID) for seq in encoding.to_int(c.variants)])
    p.scatter(x=x + i / 6, y=y, radius=0.15, fill_alpha=0.8, line_color=None, fill_color=c.color,
              legend=c.label)

p.legend.location = "top_right"
p.legend.click_policy = "hide"
show(p)

reset_output()
output_file('demo/lattice_proteins/pairwise_hist.html')

# Check pairwise B6 distance distribution
int_test = encoding.to_int(test_dataset)
n = len(int_test)
ground_d2d = pairwise_dist_hist(int_test, np.zeros((n, n))).flatten()
ground_d2d = ground_d2d[ground_d2d > 0]

variants = encoding.to_int(ae_variants_30)
variants_d2d = pairwise_dist_hist(variants, np.zeros((len(variants), len(variants)))).flatten()
variants_d2d = variants_d2d[variants_d2d > 0]

hist0, edges0 = np.histogram(ground_d2d, bins=50)
hist1, edges1 = np.histogram(variants_d2d, bins=50)

p = figure(title='Variants Pairwise Distances Histogram', x_axis_label='Distance B62', width=800)
p.quad(top=hist0, bottom=0, left=edges0[:-1], right=edges0[1:], alpha=0.7, color='blue', legend='Original Dataset')
p.quad(top=hist1, bottom=0, left=edges1[:-1], right=edges1[1:], alpha=0.7, color='red', legend='Autoencoder Variants')
p.xgrid.grid_line_color = None
p.y_range.start = 0

show(p)
