import numpy as np
from utils import encoding
from numba import jit

q = 20  # number of colors
N = 27  # number of sites on the protein
nb_structures = 10000  # total number of structures in competition

contact_map = np.load('demo/lattice_proteins/contact_map.npy')
MJ_energies = np.load('demo/lattice_proteins/Miyazawa-Jernigan_matrix.npy')
B62 = np.load('utils/bio/blosum62.npy')


# A -> 624
# B -> 847
# C -> 6682
# D -> 6684


@jit(nopython=True)
def folding_score(seq_int, structure_id):
    # seq is int !
    e_i = 0
    E = 0
    for i in range(nb_structures):
        v = 0
        for j in range(28):
            v = v + MJ_energies[seq_int[contact_map[i, j, 0]], seq_int[contact_map[i, j, 1]]]
        E += np.exp(-v)
        if structure_id == i:
            e_i = np.exp(-v)
    return -np.log(1 - (e_i / E))


# closest sequence
@jit(nopython=True)
def cns(seq, seqlist):
    m = np.inf
    for k in range(len(seqlist)):
        m_ = 0
        for i in range(27):
            if seq[i] != seqlist[k][i]:
                m_ += 1
        m = min(m, m_)
    return m

# closest B62 sequence
@jit(nopython=True)
def cns_b62(seq, seqlist):
    # returns b62 distance to all members of seqlist
    n = len(seqlist)
    l = len(seq)
    m = np.inf
    for i in range(n):
        b62 = 0
        for j in range(l):
            c = seqlist[i][j]  # int character
            b62 = b62 + B62[c][c] - B62[seq[j]][c]
        m = min(m, b62)
    return m


@jit(nopython=True)
def pairwise_dist_hist(seqlist, dists):
    # size of dists should be n*n
    n = len(seqlist)
    l = len(seqlist[0])
    for i in range(n):
        for j in range(n):
            b62 = 0
            for k in range(l):
                b62 = b62 + B62[seqlist[i][k]][seqlist[i][k]] - B62[seqlist[i][k]][seqlist[j][k]]
            dists[i, j] = b62
    return dists
