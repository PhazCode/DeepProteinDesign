import os
import sys
import time
import json
import numpy as np
import pandas as pd
from tqdm import tqdm
from collections import Counter

seqlist = pd.read_csv('studies/E_lattice-proteins_old/MSA_A.csv').sequence
seqlist = seqlist[:46080]  # so that 10% dataset is a multiple of 32
from generative_models import autoencoders as ae
from utils import sequence

intermediate_dim = 200
latent_dim = 30
dropout = 0
epochs = 200

# Train a Basic Autoencoder and save weights
model = ae.BasicVAE(intermediate_dim=intermediate_dim, latent_dim=latent_dim, dropout=dropout)
weights = 1 / sequence.position_frequencies(seqlist)
model.compile(weights=weights)
model.fit(seqlist=seqlist, epochs=epochs, batch_size=32, verbose=2)

model.save_weights('demo/lattice_proteins/weights/basicvae_MSA=A_i={0}_l={1}_d={2}_rw.weights'.format(
    intermediate_dim, latent_dim, dropout))
