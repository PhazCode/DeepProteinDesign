import numpy as np

from generative_models.hmm import HMM

hmm = HMM().from_stockholm('DB/PFAM-LL/PF00296_alignment.stklm')

# generate sample
# This has some problems
p, states = hmm.generate()

# Check a score
from utils import luxa_seq

print('LuxA log score :', hmm.viterbi_score(luxa_seq))
