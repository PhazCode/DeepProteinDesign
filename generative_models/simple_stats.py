"""
Model to create new proteins based on descriptive statistics

"""
import numpy as np
from collections import Counter
from utils.encoding import aa_letters, aa_ids
import json


class Mutator:
    def variant(self, seq: str, arg=0.1):
        return ''.join([np.random.choice(aa_letters, p=self.p(i, aa, arg)) for i, aa in enumerate(seq)])

    def p(self, i, aa, arg):
        return 21 * [1 / 21]


class SimpleStats(Mutator):
    def __init__(self, seqlist):
        p = sum([Counter(x) for x in seqlist], Counter())
        s = sum(p.values())
        self.aa_frq = np.array([p[aa_letters[i]] / s for i in range(len(aa_letters))])
        super().__init__()

    def p(self, i, aa, epsilon):
        p = epsilon * self.aa_frq / (1 - self.aa_frq[aa_ids[aa]])
        p[aa_ids[aa]] = 1 - epsilon
        return p


class MsaStats(Mutator):
    def __init__(self, seqlist):
        self.msa_table = np.array([list(x) for x in seqlist]).T
        self.nb_positions, self.nb_prots = self.msa_table.shape
        self.position_stats = [Counter(pos) for pos in self.msa_table]
        self.position_frq = [[c[aa] / self.nb_prots for aa in aa_letters] for c in self.position_stats]

    def p(self, i, aa, epsilon):
        p = epsilon * np.array(self.position_frq[i]) / (1 - self.position_frq[i][aa_ids[aa]])
        p[aa_ids[aa]] = 1 - epsilon
        return p

    def sample(self):
        return ''.join([np.random.choice(aa_letters, p=pfrq) for pfrq in self.position_frq]).replace('-', '')


class ThermalMsaStats(MsaStats):
    def p(self, i, aa, epsilon):
        temps = np.array(self.position_frq[i])
        temps = temps ** epsilon / np.sum(temps ** epsilon)  # epsilon is inverse temperature
        return temps


class BlosumStats(Mutator):
    b62_p = np.load('utils/bio/blosum62_probas.npy')

    def p(self, i, aa, epsilon):  # changes with proba epsilon
        p = BlosumStats.b62_p[aa_ids[aa]].copy()
        i, m = np.argmax(p), np.max(p)
        p[p != m] = (p[p != m] * epsilon / (1 - m))
        p[i] = 1 - epsilon
        return p
