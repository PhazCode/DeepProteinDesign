from tqdm import tqdm
from utils import encoding, aa_letters
from keras.optimizers import Adam
from keras.callbacks import TensorBoard
from keras.models import Model
from utils.layers import Conv1D, _activation, Deconv1D, WaveNet
from keras.layers import Input, Concatenate, Reshape, Dense, Add, Activation, Flatten
from keras import backend as K
import tensorflow as tf
from keras.layers import Lambda
import numpy as np
from keras import objectives
from keras.utils import multi_gpu_model

nchar = len(aa_letters)  # = 21
from utils import ml


def encoder(num_filters, kernel_size, original_dim, nchar, latent_dim, BN=True, activation="prelu", dropout=None):
    x = Input((original_dim, nchar))
    conditions = Input((6,))

    h = Conv1D(num_filters, kernel_size, activation=activation, strides=1, use_bias=not BN, BN=BN, dropout=dropout)(x)
    h = Conv1D(num_filters * 2, kernel_size, activation=activation, strides=2, use_bias=not BN, BN=BN, dropout=dropout)(
        h)
    h = Conv1D(num_filters * 4, kernel_size, activation=activation, strides=2, use_bias=not BN, BN=BN, dropout=dropout)(
        h)
    h = Conv1D(num_filters * 8, kernel_size, activation=activation, strides=2, use_bias=not BN, BN=BN, dropout=dropout)(
        h)
    h = Conv1D(num_filters * 16, kernel_size, activation=activation, strides=2, use_bias=not BN, BN=BN,
               dropout=dropout)(h)
    h = Flatten()(h)

    h_cond = Dense(6)(conditions)
    h_cond = _activation(activation, BN=False)(h_cond)
    h_cond = Dense(6)(h_cond)
    h_cond = _activation(activation, BN=False)(h_cond)
    h_cond = Dense(h._keras_shape[-1])(h_cond)
    h_cond = _activation(activation, BN=False)(h_cond)

    h = Concatenate()([h, h_cond])

    z_mean = Dense(latent_dim)(h)
    z_var = Dense(latent_dim, activation="softplus")(h)

    E = Model([x, conditions], [z_mean, z_var])
    return E


def generator(n_gates, original_dim, nchar, latent_dim, intermediate_dim, num_filters, kernel_size, activation="prelu",
              BN=True, dropout=None):
    """h_where et h_what"""
    # transformation of the latent vector: dense and deconv
    latent_vector = Input((latent_dim,))
    conditions = Input((6,))

    h_what = Dense(original_dim * nchar * 2)(Concatenate(-1)([latent_vector, conditions]))
    h_what = Reshape((original_dim, nchar * 2))(h_what)

    h_where = Dense(intermediate_dim * num_filters * 32)(latent_vector)
    h_where = Reshape((intermediate_dim, num_filters * 32))(h_where)
    h_where = Deconv1D(num_filters * 16, kernel_size, strides=2, activation=activation, use_bias=not BN, BN=BN,
                       dropout=dropout)(h_where)
    h_where = Deconv1D(num_filters * 8, kernel_size, strides=2, activation=activation, use_bias=not BN, BN=BN,
                       dropout=dropout)(h_where)
    h_where = Deconv1D(num_filters * 4, kernel_size, strides=2, activation=activation, use_bias=not BN, BN=BN,
                       dropout=dropout)(h_where)
    h_where = Deconv1D(num_filters * 2, kernel_size, strides=1, activation=activation, use_bias=not BN, BN=BN,
                       dropout=dropout)(h_where)

    h = Concatenate(2)([h_what, h_where])

    # gated convolutions
    input_prot = Input((original_dim, nchar))
    prot, skip = WaveNet(kernel_size, nchar, "a", dropout=dropout, dilation_rate=(1, 2), BN=BN, activation=activation)(
        input_prot, h)
    skip_connections = [skip]

    for k in range(n_gates):
        prot, skip = WaveNet(kernel_size, nchar, "b", dropout=dropout,
                             dilation_rate=(2 ** (2 * k + 2), 2 ** (2 * k + 3)), BN=BN, activation=activation)(prot, h)
        skip_connections.append(skip)

    prot = Add()(skip_connections)

    prot = Conv1D(num_filters, 1, activation=activation, use_bias=not BN, BN=BN, dropout=dropout)(prot)

    x_decoded_mean = Activation("softmax")(prot)
    G = Model([latent_vector, conditions, input_prot], x_decoded_mean)
    return G


def sampler(batch_size, latent_dim, epsilon_std=1):
    _sampling = lambda z_args: (z_args[0] + K.sqrt(tf.convert_to_tensor(z_args[1] + 1e-8, np.float32)) *
                                K.random_normal(shape=(batch_size, latent_dim), mean=0., stddev=epsilon_std))

    return Lambda(_sampling, output_shape=(latent_dim,))


def _vae_loss(x, x_decoded_mean, z_mean, z_var):
    xent = objectives.categorical_crossentropy(x, x_decoded_mean)
    # print(xent)
    xent_loss = K.sum(xent, -1)
    kl_loss = - 0.5 * K.sum(1 + K.log(z_var + 1e-8) - K.square(z_mean) - z_var, axis=-1)
    return xent_loss + kl_loss


def vae_loss(z_mean, z_var):
    return lambda x, x_decoded: _vae_loss(x, x_decoded, z_mean, z_var)


class ProtCNN:
    def __init__(self, original_dim=504, latent_dim=100, intermediate_dim=63,
                 num_filters=nchar, kernel_size=2, BN=True, batch_size=32, sampling_std=1, logdir=''):
        self.original_dim = original_dim
        self.latent_dim = latent_dim
        self.logdir = 'generative_models/logdir/tb/' + logdir
        self.E = encoder(num_filters, kernel_size, original_dim,
                         nchar, latent_dim, BN=BN, activation="prelu")
        self.S = sampler(batch_size, latent_dim, epsilon_std=sampling_std)
        self.G = generator(2, original_dim, nchar, latent_dim,
                           intermediate_dim, num_filters, kernel_size,
                           activation="prelu", BN=BN)
        prot, conditions = self.E.inputs  # entry tensors
        z_mean, z_var = self.E([prot, conditions])
        z = self.S([z_mean, z_var])  # sampler
        decoded = self.G([z, conditions, prot])
        self.VAE = Model(inputs=[prot, conditions], outputs=decoded)

        def xent_loss(x, x_d_m):
            return K.sum(objectives.categorical_crossentropy(x, x_d_m), -1)

        def kl_loss(x, x_d_m):
            return - 0.5 * K.sum(1 + K.log(z_var + 1e-8) - K.square(z_mean) - z_var, axis=-1)

        def vae_loss(x, x_d_m):
            return xent_loss(x, x_d_m) + kl_loss(x, x_d_m)

        # self.loss = vae_loss(z_mean, z_var)
        self.VAE.compile(loss=vae_loss,
                         optimizer=Adam(), metrics=['accuracy', xent_loss, kl_loss])
        print('ProtCNN initialized !')

    def load_weights(self, file='generative_models/weights/default.h5'):
        self.VAE.load_weights(file)
        print('Weights loaded !')
        return self

    def save_weights(self, file='generative_models/weights/default.h5'):
        self.VAE.save_weights(file)
        print('Weights saved !')
        return self

    def fit_generator(self, generator, steps_per_epoch, epochs,
                      validation_data, validation_steps, verbose):
        # steps_epochs = nb steps before 1 epoch is completed
        # validation_steps = nb times to retrieve from val_generator in 1 epoch
        tb = TensorBoard(log_dir=self.logdir)
        history = self.VAE.fit_generator(generator=generator,
                                         steps_per_epoch=steps_per_epoch,
                                         verbose=verbose,
                                         epochs=epochs,
                                         callbacks=[tb],
                                         validation_data=validation_data,
                                         validation_steps=validation_steps)
        print('Training Completed !')
        return history

    def evaluate_generator(self, test_generator, steps):
        # steps_epochs = nb steps before 1 epoch is completed
        # validation_steps = nb times to retrieve from val_generator in 1 epoch
        raw_acc, aa_acc = [], []
        for i, batch in zip(range(steps), test_generator):
            reconstruted = self.VAE.predict_on_batch(batch[0])
            x, fx = np.argmax(batch[1], axis=-1), np.argmax(reconstruted, axis=-1)
            raw_acc.append(np.mean(x == fx))
            aa_acc.append(np.sum((x == fx) * (x != 0)) / np.sum(x != 0))
        return np.mean(raw_acc), np.mean(aa_acc)

    def train(self, sequences, properties, epochs=50):
        tb = TensorBoard(log_dir=self.logdir)
        train_gen = ml.one_hot_generator(seqlist=np.array(sequences),
                                         conditions=np.array(properties),
                                         padding=504)
        val_gen = ml.one_hot_generator(seqlist=np.array(sequences),
                                       conditions=np.array(properties),
                                       padding=504)
        self.VAE.fit_generator(generator=train_gen,
                               steps_per_epoch=len(sequences) // 32,
                               verbose=2,
                               epochs=epochs,
                               callbacks=[tb],
                               validation_data=val_gen,  # fake validation
                               validation_steps=1)

    def predict(self, seqlist, condlist, remove_gaps=True, encode=True):
        n = len(seqlist)
        condlist = np.array(condlist)
        prots_oh = encoding.to_one_hot(encoding.pad(np.array(seqlist), self.original_dim))
        k = 32 - n % 32  # nb lines to add to have a number divisible by 32
        prots_oh = np.concatenate((prots_oh, np.zeros((k, self.original_dim, nchar))))
        condlist = np.concatenate((condlist, np.zeros((k, 6))))
        reconstructed = self.VAE.predict([prots_oh, condlist], batch_size=32)[:-k]
        if encode:
            return encoding.to_string(reconstructed, remove_gaps=remove_gaps)
        else:
            return reconstructed

    def evaluate(self, seqlist, condlist):
        n = len(seqlist)
        condlist = np.array(condlist)
        prots_oh = encoding.to_one_hot(encoding.pad(np.array(seqlist), self.original_dim))
        k = 32 - n % 32  # nb lines to add to have a number divisible by 32
        prots_oh = np.concatenate((prots_oh, np.zeros((k, self.original_dim, nchar))))
        condlist = np.concatenate((condlist, np.zeros((k, 6))))
        reconstructed = self.VAE.predict([prots_oh, condlist], batch_size=32)[:-k]
        x, fx = np.argmax(prots_oh[:-k], axis=-1), np.argmax(reconstructed, axis=-1)
        aa_acc = np.sum((x == fx) * (x != 0), axis=1) / np.sum(x != 0, axis=1)
        return aa_acc

    def variant(self, seq, conditions, stddev=0, proba_map=None, remove_gaps=True):
        """
        :param prot_oh: protein, one hot format.
        :param conditions: macro conditions, normalized
        :param epsilon: to move in the latent space
        :return: a sequence reconstructed with the model
        """
        prot_oh = encoding.to_one_hot(encoding.pad([seq], self.original_dim))
        conditions = np.array(conditions).reshape(1, 6)
        z_mean, z_var = self.E.predict([prot_oh, conditions])  # latent space [z_mean, z_var]
        # Sampling in latent space
        z_sample = z_mean + np.sqrt(stddev * z_var) * np.random.randn(self.latent_dim)
        return self.reconstruct(z_sample, conditions, proba_map, remove_gaps)

    def reconstruct(self, z, conditions, proba_map=None, remove_gaps=True):
        """
        reconstruct a protein from a latent point and conditions.
        :param z:
        :param conditions:
        :param proba_map:
        :param remove_gaps:
        :return:
        """
        conditions = np.array(conditions).reshape(1, 6)
        z = np.array(z).reshape(1, self.latent_dim)
        x = np.zeros((1, self.original_dim, nchar))
        for i in range(0, self.original_dim):
            p = self.G.predict([z, conditions, x])[:, i, :]  # probability vector for amino-acids
            aa = np.random.choice(list(range(21)), p=proba_map(p, i)) if proba_map is not None else np.argmax(p)
            x[0, i, aa] = 1
        return encoding.to_string(x, remove_gaps=remove_gaps)[0]

    def sample(self, n_samples=1, mean=0, stddev=1):
        """
        sample variant in the whole latent space
        :param n_samples:
        :param mean:
        :param stddev:
        :return:
        """
        z_sample = mean + stddev * np.random.randn(n_samples, self.latent_dim)
        conditions = np.random.randn(n_samples, 6)
        x = np.zeros((self.latent_dim, self.original_dim, nchar))
        start = 0
        for i in tqdm(range(start, self.original_dim)):
            pred = self.G.predict([z_sample, conditions, x])
            pred = pred[:, i, :]
            pred = pred.argmax(-1)
            for j, p in enumerate(pred):
                x[j, i, p] = 1
        seq = encoding.to_string(x)[0]
        return seq
