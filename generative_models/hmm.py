from generative_models.backend.janmaxprofilehmm import profileHMM
from Bio import AlignIO
import numpy as np
import pickle


class HMM:
    def __init__(self):
        self.name = None
        self.alphabet = None
        self.transmissions = None
        self.emissions_from_M = None
        self.emissions_from_I = None

    def from_stockholm(self, msa_file):
        msa_name = msa_file[msa_file.rfind('/'):msa_file.rfind('.')]
        with open(msa_file, 'r') as f:
            alignment = AlignIO.read(f, format='stockholm')
        MSA = np.array([list(str(p.seq).strip()) for p in alignment])
        hmm = profileHMM(MSA)
        self.name = msa_name
        self.alphabet = hmm.MSAchar
        self.transmissions = hmm.transmissions
        self.emissions_from_M = hmm.emissions_from_M
        self.emissions_from_I = hmm.emissions_from_I
        return self

    def generate(self):
        """ generate a random protein based on the B_HMM profile
         simulates a path in the B_HMM """
        # uses hmm.alphabet, hmm.emissions_from_I, hmm.emissions_from_M. hmm.transmissions
        sz, nb_states = self.transmissions.shape  # sz is the number of amino acids and n the number of states
        idx = 0  # index of state in the B_HMM
        state = 'I'  # either M, D or I, to say in what kind of state we are in
        all_states = [state]
        state_probas_location = {'M': [0, 1, 2], 'I': [3, 4, 5], 'D': [6, 7, 8]}
        new_states = {'M': ['M', 'D', 'I'], 'I': ['M', 'I', 'D'], 'D': ['M', 'D', 'I']}
        generated_protein = ''
        while idx < nb_states:
            # -1 is because emitted_from_M tab is smaller by one
            emitted_aa = np.random.choice(self.alphabet, p=self.emissions_from_M[:, idx - 1]) if state == 'M' \
                else np.random.choice(self.alphabet, p=self.emissions_from_I[:, idx]) if state == 'I' else ''
            generated_protein += emitted_aa
            proba_location = state_probas_location[state]
            state = np.random.choice(new_states[state], p=self.transmissions[proba_location, idx])
            idx += 0 if state == 'I' else 1
            all_states.append(state)
        return generated_protein, all_states

    def viterbi_score(self, prot):
        char_to_int = {c: i for i, c in enumerate(self.alphabet)}
        testdata = np.array([char_to_int[c] for c in prot.strip()])
        return profileHMM._viterbi(
            testdata,
            self.emissions_from_M,
            self.emissions_from_I,
            self.transmissions,
            len(self.emissions_from_M),
            1 / len(self.alphabet))
