import pandas as pd
from generative_models.protcnn import ProtCNN
import numpy as np
from utils import encoding


lux_a = "MKFGNFLLTYQPPQFSQTEVMKRLVKLGRISEECGFDTVWLLEHHFTEFGLLGNPYVAAAYLLGATKKLNVGTAAIVLPTAHPVRQLEDVNLLDQMSKGRFRFGICRGLYNKDFRVFGTDMNNSRALAECWYGLIKNGMTEGYMEADNEHIKFHKVKVNPAAYSRGGAPVYVVAESASTTEWAAQFGLPMILSWIINTNEKKAQLELYNEVAQEYGHDIHNIDHCLSYITSVDHDSIKAKEICRKFLGHWYDSYVNATTIFDDSDQTRGYDFNKGQWRDFVLKGHKDTNRRIDYSYEINPVGTPQECIDIIQKDIDATGISNICCGFEANGTVDEIIASMKLFQSDVMPFLKEKQRSLLY"
lux_b = "MKFGLFFLNFINSTTVQEQSIVRMQEITEYVDKLNFEQILVYENHFSDNGVVGAPLTVSGFLLGLTEKIKIGSLNHIITTHHPVRIAEEACLLDQLSEGRFILGFSDCEKKDEMHFFNRPVEYQQQLFEECYEIINDALTTGYCNPDNDFYSFPKISVNPHAYTPGGPRKYVTATSHHIVEWAAKKGIPLIFKWDDSNDVRYEYAERYKAVADKYDVDLSEIDHQLMILVNYNEDSNKAKQETRAFISDYVLEMHPNENFENKLEEIIAENAVGNYTECITAAKLAIEKCGAKSVLLSFEPMNDLMSQKNVINIVDDNIKKYHMEYT"

df = normalized = pd.read_csv('DB/IPR_LL/seb_normalized.csv')

condA = df[df['seq'] == lux_a][["charge", "iso", "mol_weight",
                                "length", "mol_extinc", "proba_ib"]].values[0]
condB = df[df['seq'] == lux_b][["charge", "iso", "mol_weight",
                                "length", "mol_extinc", "proba_ib"]].values[0]

print(condA, condB)

seb_model = ProtCNN().load_weights()
variant_a = seb_model.variant(lux_a, condA)
variant_b = seb_model.variant(lux_b, condB, stddev=0.5)
print('generated : ', variant_b)
