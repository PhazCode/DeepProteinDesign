There are several trained models here.


default.h5 : default generative model to use. Latent dim: 100, split : 95% random training
the test proteins are in DB/IPR_LL/default_test_proteins.json

tcl=luxa_ld=100: Test cluster = LuxA's cluster

train_small_clusters_ld100.h5 : training on clusters of size < 200

unique_proteins: training with only one protein per cluster90

test_clusters.h5 : training by removing some random cluster50 out of the training set.
The removed proteins are in DB/IPR_LL/test_clusters.json

