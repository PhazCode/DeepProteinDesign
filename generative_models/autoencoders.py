import numpy as np
import tensorflow as tf
from keras.layers import Input, Dense, LocallyConnected1D, Dropout, Flatten, UpSampling1D, Reshape, Lambda
from keras.models import Sequential
from keras import Model
from utils import encoding
from keras.callbacks import EarlyStopping, TensorBoard
from keras import backend as K
from keras import objectives
from utils import sequence
from utils import ml


class AutoEncoder:
    """
    basic vae with 4 layers encoding, 4 layers decoding
    """

    def __init__(self):
        self.weights = None
        self.AE = None

    def save_architecture(self, file='generative_models/architectures/tmp.arch'):
        import json
        with open(file, 'w') as f:
            json.dump(self.AE.to_json(), f)
        print('Architecture saved !')

    def save_weights(self, file='generative_models/weights/tmp.weights'):
        self.AE.save_weights(file, overwrite=True)
        print('Weights saved !')

    def load_weights(self, file):
        self.AE.load_weights(file)
        print('Weights loaded !')
        return self

    def loss(self, target, output):
        _epsilon = tf.convert_to_tensor(K.epsilon(), dtype=output.dtype.base_dtype)
        output = tf.clip_by_value(output, _epsilon, 1. - _epsilon)
        return - tf.reduce_sum(target * tf.log(output) * self.weights, axis=[-2, -1])

    @staticmethod
    def reconstruction_accuracy(x_true, x_pred):
        return K.mean(K.cast(K.equal(K.argmax(x_true, axis=-1),
                                     K.argmax(x_pred, axis=-1)), K.floatx()))

    def compile(self, weights=None):
        # modify loss to take into account the repartition of aa.
        # computes aa frq
        self.weights = weights if weights is not None else 1
        self.AE.compile(loss=self.loss,
                        optimizer='adam', metrics=[self.reconstruction_accuracy])

    def fit(self, seqlist, epochs=1, batch_size=32, validation_split=0.1, verbose=1):
        # seqlist must be a list of string
        seqs_oh = encoding.to_one_hot(seqlist)
        self.AE.fit(seqs_oh, seqs_oh, epochs=epochs, batch_size=batch_size,
                    validation_split=validation_split,
                    callbacks=[EarlyStopping(patience=3)],
                    verbose=verbose)
        # self.save_weights()

    def predict(self, seqlist):
        seqmat = self.AE.predict(encoding.to_one_hot(seqlist))
        return encoding.to_string(seqmat)

    def evaluate(self, seqlist, batch_size=32):
        seqmat = encoding.to_one_hot(seqlist)
        return self.AE.evaluate(seqmat, seqmat, batch_size=batch_size)


class VAE(AutoEncoder):
    def __init__(self, intermediate_dim, latent_dim, epsilon_std):
        super().__init__()
        self.latent_dim = latent_dim  # don't change it will break the nn
        self.intermediate_dim = intermediate_dim
        self.epsilon_std = epsilon_std
        self.z_mean, self.z_log_sigma = None, None
        self.weights = 1
        self.encoder, self.decoder = None, None

    def sampler(self, args, batch_size=32):
        z_mean, z_log_sigma = args
        epsilon = K.random_normal(shape=(batch_size, self.latent_dim), mean=0., stddev=self.epsilon_std)
        return z_mean + K.exp(z_log_sigma) * epsilon

    def loss(self, target, output):
        _epsilon = tf.convert_to_tensor(K.epsilon(), dtype=output.dtype.base_dtype)
        output = tf.clip_by_value(output, _epsilon, 1. - _epsilon)
        xent_loss = - tf.reduce_sum(target * tf.log(output) * self.weights, axis=[-2, -1])
        kl_loss = - 0.5 * K.mean(1 + self.z_log_sigma - K.square(self.z_mean) - K.exp(self.z_log_sigma), axis=-1)
        return xent_loss + kl_loss

    def variant(self, seq, epsilon_std=1):
        seq_bin = encoding.to_one_hot([seq])
        z_mean, z_log_sigma = self.encoder.predict(seq_bin)
        sampled = z_mean + np.exp(z_log_sigma) * epsilon_std * np.random.standard_normal(z_mean.shape)
        decoded = self.decoder.predict(sampled)
        return encoding.to_string(decoded)[0]

    def sample(self, epsilon_std=1):
        z = np.random.standard_normal((1, self.latent_dim)) * epsilon_std
        decoded = self.decoder.predict(z)
        return encoding.to_string(decoded)[0]


class OneLayer(AutoEncoder):
    """
    basic vae with 1 hidden layer
    """

    def __init__(self, sequences_length=27, dropout=0.1, latent_dim=50, kernel_regularizer='l1'):
        super().__init__()
        self.latent_dim = latent_dim  # don't change it will break the nn
        self.encoder = Sequential([
            Flatten(input_shape=(sequences_length, 21)),
            Dense(self.latent_dim, activation='relu', kernel_regularizer=kernel_regularizer),
        ])
        # now is time to decode
        # the tricky part is upsampling, nothing should be changed because padding=same doesnt exist
        self.decoder = Sequential([
            Dropout(dropout, input_shape=(self.latent_dim,)),
            Dense(27 * 21, activation='softmax', kernel_regularizer=kernel_regularizer),
            Reshape((27, 21)),
        ])

        self.AE = Model(inputs=self.encoder.inputs, outputs=self.decoder(self.encoder.outputs))
        self.AE.summary()


class BasicVAE(VAE):
    """
    basic vae with 1 hidden layer, variationnal
    """

    def __init__(self, sequences_length=27, dropout=0.1,
                 intermediate_dim=50, latent_dim=50, epsilon_std=1, kernel_regularizer='l1'):
        super().__init__(intermediate_dim, latent_dim, epsilon_std)
        inputs = Input(shape=(sequences_length, 21))
        h = Flatten()(inputs)
        h = Dense(self.intermediate_dim, activation='relu',
                  kernel_regularizer=kernel_regularizer)(h)
        self.z_mean = Dense(self.latent_dim)(h)
        self.z_log_sigma = Dense(self.latent_dim)(h)
        z = Lambda(self.sampler)([self.z_mean, self.z_log_sigma])

        self.decoder = Sequential([
            Dense(self.intermediate_dim, activation='relu', kernel_regularizer=kernel_regularizer,
                  input_shape=(self.latent_dim,)),
            Dense(27 * 21, activation='softmax', kernel_regularizer=kernel_regularizer),
            Reshape((27, 21))
        ])
        d = self.decoder(z)
        # models
        self.encoder = Model(inputs, [self.z_mean, self.z_log_sigma])
        self.AE = Model(inputs, d)
        self.AE.summary()
