import os
import sys
import time
import json
import numpy as np
import pandas as pd
from tqdm import tqdm
from collections import Counter

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


def reorganize(d, nb_steps=1000000):
    # reorganize the lines of m t minimize a score
    # fix first and last element !
    # goal : find s to minimize sum_i d[s[i], s[i+1]]
    n = len(d)
    s = list(range(n))
    for _ in range(nb_steps):
        (i1, i2) = np.random.randint(1, n - 1, 2)
        ds_1 = d[s[i1 - 1], s[i1]] + d[s[i1], s[i1 + 1]] + d[s[i2 - 1], s[i2]] + d[s[i2], s[i2 + 1]]
        ds_2 = d[s[i1 - 1], s[i2]] + d[s[i2], s[i1 + 1]] + d[s[i2 - 1], s[i1]] + d[s[i1], s[i2 + 1]]
        if ds_2 < ds_1:
            s[i1], s[i2] = s[i2], s[i1]
    return s


def ss_viz(ax, sslist):
    m = max([len(x) for x in sslist])
    mat = np.full((len(sslist), m), np.nan)
    for (i, seq) in enumerate(sslist):
        for (j, c) in enumerate(seq):
            mat[i, j] = 0 if c == 'H' else 1 if c == 'C' else 2 if c == '-' else 3
    cmap = plt.cm.coolwarm
    legend_lines = [Line2D([0], [0], color=cmap(0.), lw=4),
                    Line2D([0], [0], color=cmap(.33), lw=4),
                    Line2D([0], [0], color=cmap(.66), lw=4),
                    Line2D([0], [0], color=cmap(1.), lw=4)]
    ax.matshow(mat, cmap=cmap, aspect=0.7 * mat.shape[1] / mat.shape[0])
    ax.legend(legend_lines, ['Helix', 'Coil', 'Gap', 'Beta Strand'],
              loc='upper center', bbox_to_anchor=(0.5, -0.05),
              fancybox=True, shadow=True, ncol=4)
    ax.set_xlabel('Amino Acid Position')
    ax.xaxis.set_ticks_position('bottom')
