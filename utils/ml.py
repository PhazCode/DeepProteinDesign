import numpy as np
import tensorflow as tf
from keras import backend as K
from keras import objectives
from sklearn.utils import shuffle
from utils import encoding


def split_idx(n, train_ratio=0.8, val_ratio=0.1, test_ratio=0.1, seed=42):
    """ random split of range(n) """
    assert train_ratio + val_ratio + test_ratio == 1, 'What did you expect ?'
    np.random.seed(seed)
    p = np.random.permutation(n)
    i1 = int(train_ratio * n)
    i2 = int((train_ratio + val_ratio) * n)
    return p[:i1], p[i1:i2], p[i2:]


def ae_loss(target, output, weights=1):
    _epsilon = tf.convert_to_tensor(K.epsilon(), dtype=output.dtype.base_dtype)
    output = tf.clip_by_value(output, _epsilon, 1. - _epsilon)
    return - tf.reduce_sum(target * tf.log(output) * weights, axis=[-2, -1])


# generator from string
def one_hot_generator(seqlist, conditions, batch_size=32, max_size=48000, padding=504):
    n = len(seqlist)  # nb proteins
    prots_oh = None
    while True:
        # shuffle
        ps, conds = shuffle(seqlist, conditions)
        for seq_slice in np.array_split(ps, 1 + n // max_size):
            # load a maximum of max_size prot under one_hot format
            del prots_oh
            prots_oh = encoding.to_one_hot(encoding.pad(seq_slice, padding))
            for j in range(0, len(seq_slice) - batch_size, batch_size):
                batch = prots_oh[j: j + batch_size]
                yield [batch, conds[j: j + batch_size]], batch


def test_generator(seqlist, conditions, batch_size=32, padding=504):
    n = len(seqlist)
    seqlist, conditions = shuffle(seqlist, conditions)
    prots_oh = encoding.to_one_hot(encoding.pad(seqlist, padding))
    for i in range(0, n + 1 - batch_size, batch_size):
        yield [prots_oh[i:i + batch_size], conditions[i:i + batch_size]], prots_oh[i:i + batch_size]
