import numpy as np
from keras.utils import to_categorical
from tqdm import tqdm

from utils import aa_letters, aa_ids

aa_letters = ['-', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K',
              'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']
aa_ids = {l: i for i, l in enumerate(aa_letters)}


def pad(seqlist, padding=None, pad_mode='right'):
    """
    """
    if padding is None:
        return seqlist
    # ELSE we pad the prot
    assert isinstance(padding, int), 'Unknown format for argument padding'
    padded_seqlist = seqlist
    pad_char = '-' if isinstance(seqlist[0], str) else [0] if isinstance(seqlist[0], list) else None
    if pad_mode == 'right':
        return [seq + pad_char * (padding - len(seq)) for seq in seqlist]
    elif pad_mode == 'random':
        pass  # TODO


def unpad(seq):
    return ''.join([c for c in seq if (c != ' ' and c != 0)])


def to_one_hot(seqlist):
    if isinstance(seqlist[0], str):
        return to_categorical([[aa_ids[aa] for aa in seq] for seq in seqlist], num_classes=21)
    else:
        try:
            return to_categorical(seqlist, num_classes=21)
        except:
            raise ValueError('Bad formatting my boi')


def to_string(seqmat, remove_gaps=True):
    #  assumes seqmat is a matrix onehot encoded
    a = [''.join([aa_letters[np.argmax(aa)] for aa in seq]) for seq in seqmat]
    return [x.replace('-', '') for x in a] if remove_gaps else a


def to_int(seqlist, padding=None):
    seqlist = [pad(seq, padding) for seq in seqlist]
    return np.array([[aa_ids[aa] for aa in seq] for seq in tqdm(seqlist)], dtype='uint8')


def to_chemical_properties(seqlist, padding=None):
    seqlist = [pad(seq, padding) for seq in seqlist]
    embedding = np.load('utils/bio/aa_embedding.npy')
    return np.array([np.array([embedding[aa_ids[aa]] for aa in seq]).T for seq in seqlist])


def to_fasta(seqlist, filename):
    with open(filename, 'w') as f:
        f.write(''.join(['>{0}\n{1}\n'.format(i, p) for i, p in enumerate(seqlist)]))


from Bio import SeqIO


def load_fasta(filename):
    return [str(x.seq) for x in SeqIO.parse(filename, 'fasta')]
