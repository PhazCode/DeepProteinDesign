"""
This file defines several useful custom layers (these are not actual keras.layers.Layer objects) to be used in the network
You can call them like this:
    `LayerName(...)(x)`

This is useful since very often you need to chain some layers (like the classical Conv + BatchNorm + NonLinearity)
"""

import tensorflow as tf
from keras.layers import Dense as kDense, MaxPooling1D, PReLU, ELU, LeakyReLU, Activation, Permute, Conv2DTranspose, \
    Conv1D as kConv1D, BatchNormalization, Add, Concatenate, Multiply, Dropout, merge, Reshape, Flatten, UpSampling1D, \
    Lambda, \
    ZeroPadding1D
from keras import backend as K
import numpy as np
import tqdm

BATCH_NORM = 'keras'


# CONVOLUTIONS
def inception_layer(list_of_lists_of_layers_names, activation="relu"):
    """
    A module to get an inception module
    For more details you can look at these papers:
        - `Going deeper with convolutions` from Google
        - `Rethinking the Inception Architecture for Computer Vision` from Google
        - https://github.com/fchollet/deep-learning-models/blob/master/inception_v3.py
        - http://joelouismarino.github.io/blog_posts/blog_googlenet_keras.htm
    
    :param activation: the activation that is used for each layer
    :param list_of_lists_of_layers_names: a list of lists of layer names
    NOTE THAT ALL LAYERS ARE ALWAYS FOLLOWED BY RELU ONLY

    These names have the following formats:
        <layer_name>_<parameters>

    The names are, for each class:
        - Convolution1D -> c or conv
        - AtrousConvolution1D -> dc or cat or convatrous or dilatedconv
        - MaxPooling1D -> mp or maxpool

    Then pass the argument values, separated with `_`

    For example, if you want a 10 filters of 3x3 convolution with stride 1 and same padding, write:
        c_10f_3l_1s_same
    Note that the order is unimportant. Here are other examples:
        c_12f_1l_1s_valid
        c_100f_10l_3s_same

    The syntax for a max pooling filter with size 2x2 and stride 1 and valid padding is:
        mp_2l_1s_valid
    note that the order is unimportant

    The syntax for 100 filters of dilated convolution of size 3x3 with stride 2 and dilation 1 and same padding is:
        dc_100f_3l_2s_1d_same

    If you want to chain layers, you can. Here is an example of what you can have:
        [
            ["c_1l_1s_10f_same", "c_3l_1s_same_10f"],
            ["mp_4l_4s"],
            ["c_4l_2s_10f_same"]
        ]
    :return: a Keras layer, resulting from the concatenation from the passed layers
    """

    def f(inputd):
        inception = []
        for list_of_layers in list_of_lists_of_layers_names:
            x = inputd
            for layer in list_of_layers:
                parameters = layer.split('_')
                if parameters[0] in ['c', "conv"]:
                    assert len(
                        parameters) == 5, "There should be 4 parameters for a conv layer : the number of filters, their size, their stride, and their padding (same/valid)"
                    padding = None
                    stride = None
                    nb_filters = None
                    size = None
                    for parameter in parameters:
                        if parameter in ['valid', 'same']:
                            padding = parameter
                        if parameter[-1] == "f":
                            nb_filters = int(parameter[:-1])
                        if parameter[-1] == "l":
                            size = int(parameter[:-1])
                        if parameter[-1] == "s":
                            stride = int(parameter[:-1])
                    assert size is not None and stride is not None and padding is not None and nb_filters is not None, "You should provide size, stride, padding and nb_filters (see function's documentation)"
                    x = Conv1D(nb_filters, size, border_mode=padding, subsample_length=stride, activation=activation)(x)
                elif parameters[0] in ['mp', "maxpool"]:
                    assert len(
                        parameters) == 4, "There should be 3 parameters for a max pooling layer: the size of the filters, the padding, and the stride"
                    stride = None
                    size = None
                    padding = None
                    for parameter in parameters:
                        if parameter in ['valid', 'same']:
                            padding = parameter
                        if parameter[-1] == 'l':
                            size = int(parameter[:-1])
                        if parameter[-1] == "s":
                            stride = int(parameter[:-1])
                    assert size is not None and stride is not None and padding is not None, "You should provide size, stride, padding (see function's documentation)"
                    x = MaxPooling1D(pool_length=size, border_mode=padding, stride=stride)(x)
                elif parameters[0] in ["dc", "cat", "convatrous", "dilatedconv"]:
                    assert len(
                        parameters) == 6, "There should be 5 parameters for a dilated conv layer : the dilation, the number of filters, their size, their stride, and their padding (same/valid)"
                    padding = None
                    stride = None
                    nb_filters = None
                    size = None
                    dilation = None
                    for parameter in parameters:
                        if parameter in ['valid', 'same']:
                            padding = parameter
                        if parameter[-1] == "f":
                            nb_filters = int(parameter[:-1])
                        if parameter[-1] == 'l':
                            size = int(parameter[:-1])
                        if parameter[-1] == "s":
                            stride = int(parameter[:-1])
                        if parameter[-1] == "d":
                            dilation = int(parameter[:-1])
                    assert size is not None and stride is not None and padding is not None and nb_filters is not None and dilation is not None, "You should provide size, stride, padding, dilation, and nb_filters (see function's documentation)"
                    x = Conv1D(nb_filters, size, border_mode=padding, subsample_length=stride, activation=activation,
                               dilation_rate=dilation)(x)
            inception.append(x)
        return merge(inception, mode="concat", concat_axis=-1)

    return f


def Conv1D(filters, kernel_size, strides=1, padding='same', dilation_rate=1, activation=None, momentum=0.9,
           training=None, BN=True, config=BATCH_NORM,
           use_bias=False, kernel_initializer='glorot_uniform', bias_initializer='zeros', kernel_regularizer=None,
           bias_regularizer=None,
           activity_regularizer=None, kernel_constraint=None, bias_constraint=None, dropout=None, name=None, **kwargs):
    """BN after AtrousConvolution1D and BEFORE activation function"""

    def f(x):
        h = x
        if dropout is not None:
            h = Dropout(dropout)(h)
        h = kConv1D(filters,
                    kernel_size,
                    strides=strides,
                    padding=padding,
                    dilation_rate=dilation_rate,
                    activation=None,
                    use_bias=use_bias,
                    kernel_initializer=kernel_initializer,
                    bias_initializer=bias_initializer,
                    kernel_regularizer=kernel_regularizer,
                    bias_regularizer=bias_regularizer,
                    activity_regularizer=activity_regularizer,
                    kernel_constraint=kernel_constraint,
                    bias_constraint=bias_constraint,
                    **kwargs)(h)
        h = _activation(activation, BN=BN, name=name, momentum=momentum, training=training, config=config)(h)
        return h

    return f


def Deconv1D(filters, kernel_size, strides=2, padding='same', dilation_rate=1, activation="prelu", momentum=0.9,
             BN=True, config=BATCH_NORM,
             use_bias=False, training=None, kernel_initializer='glorot_uniform', bias_initializer='zeros',
             kernel_regularizer=None, bias_regularizer=None,
             activity_regularizer=None, kernel_constraint=None, bias_constraint=None, dropout=None, name=None):
    """`strides` is the upsampling factor"""

    def f(x):
        shape = list(x._keras_shape[1:])
        assert len(shape) == 2, "The input should have a width and a depth dimensions (plus the batch dimensions)"
        new_shape = shape[:-1] + [1] + [shape[-1]]
        h = Reshape(new_shape)(x)

        if dropout is not None:
            h = Dropout(dropout)(h)

        h = Conv2DTranspose(filters,
                            kernel_size,
                            strides=(strides, 1),
                            padding=padding,
                            dilation_rate=dilation_rate,
                            activation=None,
                            use_bias=use_bias,
                            kernel_initializer=kernel_initializer,
                            bias_initializer=bias_initializer,
                            kernel_regularizer=kernel_regularizer,
                            bias_regularizer=bias_regularizer,
                            activity_regularizer=activity_regularizer,
                            kernel_constraint=kernel_constraint,
                            bias_constraint=bias_constraint
                            )(h)
        h = _activation(activation, BN=BN, name=name, momentum=momentum, training=training, config=config)(h)
        shape = list(h._keras_shape[1:])
        new_shape = shape[:-2] + [filters]
        h = Reshape(new_shape)(h)
        return h

    return f


def ResizedConv(filters, kernel_size=3, n=2, training=None, dropout=None, BN=True, config=BATCH_NORM):
    """
    Upsampling by concatenating convolutions
    1x1 followed by fxf conv. Usually f=3
    :param filters:
    :param kernel_size:
    :param n: the factor by which you want to increase the size
    :param dropout: dropout rate before convolutions
    :return:
    """

    def f(inputd):
        hs = []
        for k in range(n):
            h = Conv1D(filters, 1, activation=None, training=training, padding="same", dropout=dropout, BN=BN,
                       config=config)(inputd)
            h = PReLU()(h)
            h = Conv1D(filters, kernel_size, activation=None, training=training, padding="same", dropout=dropout, BN=BN,
                       config=config)(h)
            h = PReLU()(h)
            hs.append(h)
        h = Concatenate(1)(hs)
        return h

    return f


def ResConv1D(filters, kernel_size, strides=1, last_activation=True, BN=True, padding='same', dilation_rate=1,
              activation=None, config=BATCH_NORM, use_bias=False,
              training=None, kernel_initializer='glorot_uniform', bias_initializer='zeros', kernel_regularizer=None,
              bias_regularizer=None,
              activity_regularizer=None, kernel_constraint=None, bias_constraint=None, **kwargs):
    """
    Residual connection:
    + <----- conv kxk
    |            |
    |        conv kxk
    |            |
    |------------>
    x
    :param last_conv: whether to use the last 1x1 conv
    :param BN: whether to use BN before the activation
    """

    def f(x):
        h = kConv1D(filters,
                    kernel_size,
                    strides=strides,
                    padding=padding,
                    dilation_rate=1,
                    activation=None,
                    use_bias=use_bias,
                    kernel_initializer=kernel_initializer,
                    bias_initializer=bias_initializer,
                    kernel_regularizer=kernel_regularizer,
                    bias_regularizer=bias_regularizer,
                    activity_regularizer=activity_regularizer,
                    kernel_constraint=kernel_constraint,
                    bias_constraint=bias_constraint,
                    **kwargs)(x)
        h = _activation(activation, BN=BN, training=training, config=config)(h)
        h = kConv1D(filters,
                    kernel_size,
                    strides=1,
                    padding=padding,
                    dilation_rate=dilation_rate,
                    activation=None,
                    use_bias=use_bias,
                    kernel_initializer=kernel_initializer,
                    bias_initializer=bias_initializer,
                    kernel_regularizer=kernel_regularizer,
                    bias_regularizer=bias_regularizer,
                    activity_regularizer=activity_regularizer,
                    kernel_constraint=kernel_constraint,
                    bias_constraint=bias_constraint,
                    **kwargs)(h)
        if strides == 1 and filters == x._keras_shape[-1]:
            shortcut = x
        elif strides == 1 and x._keras_shape[-1] != filters:
            shortcut = kConv1D(filters, kernel_size, padding=padding)(x)
        elif strides != 1:
            shortcut = kConv1D(filters, kernel_size, padding=padding, strides=strides)(x)
        else:
            raise ValueError
        h = Add()([h, shortcut])
        if last_activation:
            return _activation(activation, BN=BN, training=training, config=config)(h)
        else:
            return h

    return f


def ResDeconv1D(filters, kernel_size, strides=1, last_activation=True, BN=True, padding='same', dilation_rate=1,
                activation=None, use_bias=False, config=BATCH_NORM,
                training=None, kernel_initializer='glorot_uniform', bias_initializer='zeros', kernel_regularizer=None,
                bias_regularizer=None,
                activity_regularizer=None, kernel_constraint=None, bias_constraint=None, **kwargs):
    """
    Residual connection:
    + <----- conv kxk
    |            |
    |        conv kxk
    |            |
    |------------>
    x
    :param last_conv: whether to use the last 1x1 conv
    :param BN: whether to use BN before the activation
    """

    def f(x):
        h = Deconv1D(filters,
                     kernel_size,
                     training=training,
                     strides=strides,
                     config=config,
                     padding=padding,
                     dilation_rate=1,
                     BN=BN,
                     activation=activation,
                     use_bias=use_bias,
                     kernel_initializer=kernel_initializer,
                     bias_initializer=bias_initializer,
                     kernel_regularizer=kernel_regularizer,
                     bias_regularizer=bias_regularizer,
                     activity_regularizer=activity_regularizer,
                     kernel_constraint=kernel_constraint,
                     bias_constraint=bias_constraint,
                     **kwargs)(x)
        h = Deconv1D(filters,
                     kernel_size,
                     training=training,
                     config=config,
                     strides=1,
                     padding=padding,
                     dilation_rate=dilation_rate,
                     activation=activation,
                     BN=False,
                     use_bias=use_bias,
                     kernel_initializer=kernel_initializer,
                     bias_initializer=bias_initializer,
                     kernel_regularizer=kernel_regularizer,
                     bias_regularizer=bias_regularizer,
                     activity_regularizer=activity_regularizer,
                     kernel_constraint=kernel_constraint,
                     bias_constraint=bias_constraint,
                     **kwargs)(h)
        if strides == 1 and filters == x._keras_shape[-1]:
            shortcut = x
        elif strides == 1 and x._keras_shape[-1] != filters:
            shortcut = Deconv1D(filters, kernel_size, padding=padding, training=training, BN=False, activation=None,
                                strides=1, config=config)(x)
        elif strides != 1:
            shortcut = Deconv1D(filters, kernel_size, padding=padding, training=training, strides=strides, BN=False,
                                activation=None, config=config)(x)
        else:
            raise ValueError
        h = Add()([h, shortcut])
        if last_activation:
            return _activation(activation, BN=BN, training=training, config=config)(h)
        else:
            return h

    return f


# DENSE
def Dense(n_units, activation=None, BN=False, channel=1, training=None, config=BATCH_NORM):
    def f(x):
        if len(x._keras_shape[1:]) == 2:
            if channel == 2:
                h = kDense(n_units)(x)
            elif channel == 1:
                h = Permute((2, 1))(kDense(n_units)(Permute((2, 1))(x)))
            else:
                raise ValueError('channel should be either 1 or 2')
            h = _activation(activation, BN=BN, training=training, config=config)(h)
            return h
        elif len(x._keras_shape[1:]) == 1:
            h = kDense(n_units)(x)
            return _activation(activation, BN=BN, training=training, config=config)(h)
        else:
            raise ValueError('len(x._keras_shape) should be either 2 or 3 (including the batch dim)')

    return f


# BatchNorm
def BatchNorm(momentum=0.99, training=True):
    def batchnorm(x, momentum=momentum, training=training):
        return tf.layers.batch_normalization(x, momentum=momentum, training=training)

    def f(x):
        return Lambda(batchnorm, output_shape=tuple([xx for xx in x._keras_shape if xx is not None]))(x)

    return f


# ASPP
def ASPP(filters, kernel_size, dilation_rates=[1, 2, 4], activation="prelu", BN=True, training=None, number_of_convs=3,
         padding="same", strides=1, dropout=None, kernel_regularizer=None, momentum=0.9, config=BATCH_NORM):
    """ASPP block as described in https://arxiv.org/pdf/1606.00915.pdf"""

    def f(input):
        hi = Conv1D(filters, kernel_size, activation=activation, dilation_rate=1, config=config,
                    padding=padding, dropout=dropout, kernel_regularizer=kernel_regularizer,
                    strides=strides, momentum=momentum, BN=BN, training=training)(input)
        hs = [hi]

        # Get representations at different scales
        for rate in dilation_rates:
            hi = hs[0]
            for k in range(number_of_convs):
                hi = Conv1D(filters, kernel_size, activation=activation, config=config, dilation_rate=rate, BN=BN,
                            training=training,
                            padding=padding, dropout=dropout, kernel_regularizer=kernel_regularizer, momentum=momentum)(
                    hi)
            hi = Conv1D(filters, 1, activation=None, config=config, dilation_rate=1, BN=BN, training=training,
                        padding=padding, dropout=dropout, kernel_regularizer=kernel_regularizer, momentum=momentum)(hi)
            if dropout is not None:
                hi = Dropout(dropout)(hi)
            hs.append(hi)
        h = Add()(hs)

        return h

    return f


def ASPPd(filters, kernel_size, dilation_rates=[1, 2, 4], training=None, activation="prelu", config=BATCH_NORM, BN=True,
          number_of_convs=3, strides=1, padding="same", dropout=None, kernel_regularizer=None, momentum=0.9):
    """ASPP block with deconv"""

    def f(input):
        hi = Deconv1D(filters, kernel_size, activation=activation, dilation_rate=1, config=config,
                      padding=padding, dropout=dropout, kernel_regularizer=kernel_regularizer,
                      strides=strides, momentum=momentum, BN=BN, training=training)(input)
        hs = [hi]

        # Get representations at different scales
        for rate in dilation_rates:
            hi = hs[0]
            for k in range(number_of_convs):
                hi = Deconv1D(filters, kernel_size, activation=activation, dilation_rate=rate, strides=1, BN=BN,
                              training=training, config=config,
                              padding=padding, dropout=dropout, kernel_regularizer=kernel_regularizer,
                              momentum=momentum)(hi)
            hi = Deconv1D(filters, 1, dilation_rate=1, padding=padding, dropout=dropout, strides=1, momentum=momentum,
                          config=config,
                          kernel_regularizer=kernel_regularizer, BN=BN, training=training)(hi)
            if dropout is not None:
                hi = Dropout(dropout)(hi)
            hs.append(hi)
        h = Add()(hs)

        return h

    return f


# ACTIVATION
def _activation(activation, BN=True, name=None, momentum=0.9, training=None, config=BATCH_NORM):
    """
    A more general activation function, allowing to use just string (for prelu, leakyrelu and elu) and to add BN before applying the activation
    :param training: if using a tensorflow optimizer, training should be K.learning_phase()
                     if using a Keras optimizer, just let it to None
    """

    def f(x):
        if BN and activation != 'selu':
            if config == 'keras':
                h = BatchNormalization(momentum=momentum)(x, training=training)
            elif config == 'tf' or config == 'tensorflow':
                h = BatchNorm(is_training=training)(x)
            else:
                raise ValueError('config should be either `keras`, `tf` or `tensorflow`')
        else:
            h = x
        if activation is None:
            return h
        if activation in ['prelu', 'leakyrelu', 'elu', 'selu']:
            if activation == 'prelu':
                return PReLU(name=name)(h)
            if activation == 'leakyrelu':
                return LeakyReLU(name=name)(h)
            if activation == 'elu':
                return ELU(name=name)(h)
            if activation == 'selu':
                return Selu()(h)
        else:
            h = Activation(activation, name=name)(h)
            return h

    return f


def _selu(x):
    """Scaled Exponential Linear Unit. (Klambauer et al., 2017)
    # Arguments
        x: A tensor or variable to compute the activation function for.
    # References
        - [Self-Normalizing Neural Networks](https://arxiv.org/abs/1706.02515)
    """
    alpha = 1.6732632423543772848170429916717
    scale = 1.0507009873554804934193349852946
    return scale * K.elu(x, alpha)


def Selu():
    def f(x):
        return Lambda(_selu)(x)

    return f


def gate(x, num_filters):
    x1 = x[:, :, :num_filters // 2]
    x2 = x[:, :, num_filters // 2:]
    return Multiply()([x1, Activation('sigmoid')(x2)])


# ENCODER/DECODER
def encoder_block(num_filters=50, kernel_size=3, latent_dim=100, training=None, dilation_rates=[2], BN=True,
                  activation="prelu", nb_stack=1, number_of_convs=3, skip=False, dropout=None):
    def f(x):
        h = Conv1D(num_filters, 1, training=training, activation="prelu", dropout=None)(x)

        if skip:
            skip_connections = [h]
        for i in range(nb_stack):
            h = ASPP(num_filters, kernel_size, training=training, dilation_rates=dilation_rates, BN=BN,
                     activation=activation, number_of_convs=number_of_convs, dropout=dropout)(h)
            if skip:
                skip_connections.append(h)
        if skip:
            h = Concatenate(2)(skip_connections)

        h = Conv1D(1, kernel_size, training=training, activation="prelu", BN=BN, dropout=dropout)(h)
        h = Conv1D(1, kernel_size, training=training, activation="prelu", BN=BN, dropout=dropout)(h)
        h = Flatten()(h)
        if dropout is not None:
            h = Dropout(dropout)(h)
        z_mean = kDense(latent_dim)(h)
        z_std = kDense(latent_dim, activation="softplus")(h)
        return z_mean, z_std

    return f


def decoder_block(intermediate_dim, filters, kernel_size, nb_stack, nchar, training=None, config=BATCH_NORM,
                  dilation_rates=[2], BN=True, skip=False, activation="prelu", resized_conv=True, number_of_convs=5,
                  dropout=None):
    def f(inputd):

        # get good dimension
        hd = kDense(intermediate_dim, activation=None)(inputd)
        hd = _activation(activation, BN=BN, training=training, config=config)(hd)
        hd = Reshape((intermediate_dim, 1))(hd)
        if resized_conv:
            hd = ResizedConv(filters, kernel_size=kernel_size, n=2, dropout=dropout, training=training, BN=BN,
                             config=config)(hd)
        else:
            hd = UpSampling1D()(hd)

        # Conv filters
        hd = Conv1D(filters, kernel_size, padding='same', training=training, activation=activation, dropout=dropout,
                    BN=BN, config=config)(hd)
        hd = Conv1D(filters, kernel_size, padding='same', training=training, activation=activation, dropout=dropout,
                    BN=BN, config=config)(hd)

        # ASPP blocks
        if not skip:
            for i in range(nb_stack):
                hd = ASPP(filters, kernel_size, training=training, dilation_rates=dilation_rates, activation=activation,
                          BN=BN, config=config, number_of_convs=number_of_convs, dropout=dropout)(hd)
        else:
            skip_connections = [hd]
            for i in range(nb_stack):
                hd = ASPP(filters, kernel_size, training=training, dilation_rates=dilation_rates, activation=activation,
                          BN=BN, config=config, number_of_convs=number_of_convs, dropout=dropout)(hd)
            skip_connections.append(hd)
            hd = Concatenate(axis=2)(skip_connections)

        # Final conv filter with softmax
        hd = Conv1D(nchar, 1, padding="same", training=training, activation="softmax", BN=BN, config=config)(hd)
        return hd

    return f


# AUTOREGRESSIVE MODELS
def PixelCNNGatedConv(kernel_size, num_filters, padding, activation="prelu", BN=True, residual=True, dropout=None,
                      dilation_rate=1):
    """
    PixelCNN gated convolutions
    :param residual:
    :param dropout: dropout rate at each layer
    :param dilation_rate: an integer or tuple of two integers giving the dilation rates of the first and second convolutions
    :param kernel_size:
    :param num_filters:
    :param activation:
    :param BN:
    :param padding: `a` or `b`
                    `a` if it is the first conv (in this case the neuron at the same position isn't taken into account in the computation)
                    `b` if it's  not (the neuron at the same position is taken into account in the computation)

                    Examples with a kernel size of 3:
                    `a`:
                          _  <-- to compute this time step we use the three previous ones
                    _ _ _ x

                    `b`:
                          _  <-- to compute this time step we use the two previous one and the current one
                      _ _ _
    :return:
    """

    def f(x, h):
        if isinstance(dilation_rate, tuple):
            d1, d2 = dilation_rate
        else:
            d1 = d2 = dilation_rate

        # two causal convolutions (the first one being either type a or b, the second one being b)
        if padding == "b":  # access to the past and the present, but not the future
            xx = Conv1D(num_filters, kernel_size, padding="causal", BN=BN, use_bias=not BN, activation=activation,
                        dropout=dropout, dilation_rate=d1)(x)
        elif padding == "a":  # shift to the right so that you only have access to the past
            xx = ZeroPadding1D(padding=(2, 0))(x)
            xx = Conv1D(num_filters, kernel_size, BN=BN, activation=activation, use_bias=not BN, dropout=dropout,
                        dilation_rate=d1)(xx)
            xx = Lambda(lambda x_: x_[:, :-2, :])(xx)
        else:
            raise ValueError
        xx = Conv1D(num_filters * 2, kernel_size, padding="causal", BN=BN, use_bias=not BN, activation=None,
                    dropout=dropout, dilation_rate=d2)(xx)

        # conditional vector
        h = Conv1D(num_filters * 2, 1, activation=activation, BN=BN, dropout=dropout)(h)
        xx = Add()([xx, h])

        # gate
        xx = Lambda(gate, arguments={"num_filters": 2 * num_filters})(xx)

        # add residual connexions if needed
        if residual:
            if padding == "b":  # access to the past and the present, but not the future
                x_ = x
            elif padding == "a":  # shift to the right so that you only have access to the past
                x_ = ZeroPadding1D(padding=(1, 0))(x)
                x_ = Lambda(lambda x_: x_[:, :-1, :])(x_)
            else:
                raise ValueError
            xx = Add()([xx, x_])
        return xx

    return f


def generate(model, z, original_dim, nchar, x=None, start=None):
    """Used to generate proteins with an autoregressive model (decoder using PixelCNNGatedConv)"""
    if x is None:
        x = np.zeros((z.shape[0], original_dim, nchar))
        start = 0
    for i in tqdm.tqdm(range(start, original_dim)):
        pred = model.predict([z, x])
        pred = pred[:, i, :]
        pred = pred.argmax(-1)
        for j, p in enumerate(pred):
            x[j, i, p] = 1
    return x


def WaveNet(kernel_size, num_filters, padding, dilation_rate=1, activation="prelu", BN=True, dropout=None,
            conv_on_h=True, mode=1, use_h=True):
    """
    wavenet model
    :param padding: `a` or `b`. If `a`, use only inputs up to t-1. If `b`, up to t
    :param conv_on_h: whether to convolve on h before summing to x
    :param mode: either 1 or 2. If 1, there are two causal convolutions on x. If 2, there is one only - and no activation on the last convolution
    """

    def f1(x, h):
        if isinstance(dilation_rate, tuple):
            d1, d2 = dilation_rate
        else:
            d1 = d2 = dilation_rate

        # two causal convolutions (the first one being either type a or b, the second one being b)
        if padding == "b":  # access to the past and the present, but not the future
            xx = Conv1D(num_filters, kernel_size, padding="causal", BN=BN, use_bias=not BN, activation=activation,
                        dropout=dropout, dilation_rate=d1)(x)
        elif padding == "a":  # shift to the right so that you only have access to the past
            xx = ZeroPadding1D(padding=(2, 0))(x)
            xx = Conv1D(num_filters, kernel_size, BN=BN, activation=activation, use_bias=not BN, dropout=dropout,
                        dilation_rate=d1)(xx)
            xx = Lambda(lambda x_: x_[:, :-2, :])(xx)
        else:
            raise ValueError
        xx = Conv1D(num_filters * 2, kernel_size, padding="causal", BN=BN, use_bias=not BN, activation=None,
                    dropout=dropout, dilation_rate=d2)(xx)

        # conditional vector
        if use_h:
            if conv_on_h:
                h = Conv1D(num_filters * 2, 1, activation=activation, BN=BN, use_bias=not BN, dropout=dropout)(h)
            xx = Add()([xx, h])

        # gate
        xx = Lambda(gate, arguments={"num_filters": 2 * num_filters})(xx)

        xx = Conv1D(num_filters, 1, BN=BN, use_bias=not BN, activation=activation)(xx)

        # create two outputs: out which will be passed to the next block and skip which will be summed to the others in the end
        skip = xx
        out = xx

        if padding == "b":  # access to the past and the present, but not the future
            x_ = x
        elif padding == "a":  # shift to the right so that you only have access to the past
            x_ = ZeroPadding1D(padding=(1, 0))(x)
            x_ = Lambda(lambda x_: x_[:, :-1, :])(x_)
        else:
            raise ValueError
        out = Add()([out, x_])

        return out, skip

    def f2(x, h):
        """One causal conv only on x"""
        assert isinstance(dilation_rate, int)

        # two causal convolutions (the first one being either type a or b, the second one being b)
        if padding == "b":  # access to the past and the present, but not the future
            xx = Conv1D(2 * num_filters, kernel_size, padding="causal", BN=BN, use_bias=not BN, activation=activation,
                        dropout=dropout, dilation_rate=dilation_rate)(x)
        elif padding == "a":  # shift to the right so that you only have access to the past
            xx = ZeroPadding1D(padding=(2, 0))(x)
            xx = Conv1D(2 * num_filters, kernel_size, BN=BN, activation=activation, use_bias=not BN, dropout=dropout,
                        dilation_rate=dilation_rate)(xx)
            xx = Lambda(lambda x_: x_[:, :-2, :])(xx)
        else:
            raise ValueError

        # conditional vector
        if use_h:
            if conv_on_h:
                h = Conv1D(num_filters * 2, 1, activation=activation, use_bias=not BN, BN=BN, dropout=dropout)(h)
            xx = Add()([xx, h])

        # gate
        xx = Lambda(gate, arguments={"num_filters": 2 * num_filters})(xx)

        xx = Conv1D(num_filters, 1, BN=BN, use_bias=not BN, activation=None)(xx)

        # create two outputs: out which will be passed to the next block and skip which will be summed to the others in the end
        skip = xx
        out = xx

        if padding == "b":  # access to the past and the present, but not the future
            x_ = x
        elif padding == "a":  # shift to the right so that you only have access to the past
            x_ = ZeroPadding1D(padding=(1, 0))(x)
            x_ = Lambda(lambda x_: x_[:, :-1, :])(x_)
        else:
            raise ValueError
        out = Add()([out, x_])

        return out, skip

    if mode == 1:
        return f1
    elif mode == 2:
        return f2
    else:
        raise ValueError


def _mask_matrix_made(dim):
    """A generator of masks for two-layered MADE model (see https://arxiv.org/pdf/1502.03509.pdf)"""
    mask_vector = np.random.randint(1, dim, dim)
    mask_matrix0 = np.fromfunction(lambda k, d: mask_vector[k] >= d, (dim, dim), dtype=int).astype(np.int32).astype(
        np.float32)
    mask_matrix1 = np.fromfunction(lambda d, k: d > mask_vector[k], (dim, dim), dtype=int).astype(np.int32).astype(
        np.float32)
    return mask_matrix0, mask_matrix1


def MADE(mask_matrix0, mask_matrix1, latent_dim):
    """A 2-layered MADE model (https://arxiv.org/pdf/1502.03509.pdf)"""

    def f(x):
        hl = MaskedDense(latent_dim, mask=mask_matrix0)(x)
        hl = PReLU()(hl)
        std = MaskedDense(latent_dim, mask=mask_matrix1, activation="softplus")(hl)
        mean = MaskedDense(latent_dim, mask=mask_matrix1, activation=None)(hl)
        return mean, std

    return f


class MaskedDense(kDense):
    """A dense layer with a masking possibilities"""

    def __init__(self, units, mask, activation=None, use_bias=True, kernel_initializer='glorot_uniform',
                 bias_initializer='zeros',
                 kernel_regularizer=None, bias_regularizer=None, activity_regularizer=None, kernel_constraint=None,
                 bias_constraint=None,
                 transpose=False, **kwargs):
        super(MaskedDense, self).__init__(units, bias_initializer=bias_initializer,
                                          activation=activation, kernel_initializer=kernel_initializer,
                                          kernel_regularizer=kernel_regularizer, bias_regularizer=bias_regularizer,
                                          activity_regularizer=activity_regularizer,
                                          kernel_constraint=kernel_constraint, bias_constraint=bias_constraint,
                                          use_bias=use_bias, **kwargs)
        if not transpose:
            self.mask = K.variable(mask)
        else:
            self.mask = K.variable(mask.T)

    def call(self, x, mask=None):
        output = K.dot(x, Multiply()([self.kernel, self.mask]))
        if self.use_bias:
            output = K.bias_add(output, self.bias)
        if self.activation is not None:
            output = self.activation(output)
        return output


# DENSENET
def DenseBlock(init_filters, kernel_size, n_blocks, growth_rate, activation='prelu', dropout_rate=0.5, dilation_rate=1):
    """
    A conv group is defined by conv1x1 + BN + activation + conv fxf + BN + activation
    A dense block contains several stacked conv groups
    Each conv group is connected to all the following ones
    """

    def f(x):
        concat_blocks = Conv1D(init_filters, 1, activation=activation)(x)
        for k in range(n_blocks):
            if dropout_rate:
                concat_blocks = Dropout(dropout_rate)(concat_blocks)
            h = Conv1D(4 * growth_rate, 1, activation=activation)(concat_blocks)
            h = Conv1D(growth_rate, kernel_size, activation=activation, dilation_rate=dilation_rate)(h)
            concat_blocks = Concatenate(2)([concat_blocks, h])
        return concat_blocks

    return f
