"""
This file gathers many calculations on the physical properties of a protein
"""

from Bio.SeqUtils.ProtParam import ProteinAnalysis
from Bio.SeqUtils import MeltingTemp as mt
from collections import Counter


# you can also call emboss in apis/emboss


def solubility(prot):
    """ Computes the solubility according to the formula in
    http://www.biotech.ou.edu/innovations.pdf
    note that the solubility/insolubility decision boundary lie at 0.4934
    """
    assert isinstance(prot, str), 'Please provide string for this method'
    n = len(prot)
    c = Counter(prot)
    l1, l2 = 15.43, -29.56
    cv = l1 * (c['N'] + c['G'] + c['P'] + c['S']) / n + \
         l2 * abs((c['R'] + c['K'] - c['D'] - c['E']) / n - 0.03)
    cv_ = 1.71
    proba = 0.4934 + 0.276 * abs(cv - cv_) - 0.0392 * (cv - cv_)
    return {'solubility': cv <= cv_,
            'probability': proba if cv <= cv_ else 1 - proba}


charges_of_amino_acids = dict(B=-1, D=-1, E=-1, Z=-1, K=1, R=1, H=0.5)


def physical_properties(prot):
    assert isinstance(prot, str), 'Please provide string for this method'
    analysis = ProteinAnalysis(prot)
    counter = Counter(prot)
    return {
        'sequence': prot,
        'charge': sum([counter[aa] * c for (aa, c) in charges_of_amino_acids.items()]),
        'solubility': round(solubility(prot)['probability'], 2),
        'molecular_weight': int(analysis.molecular_weight()),
        'aromaticity': round(analysis.aromaticity(), 3),
        'instability_index': round(analysis.instability_index(), 2),
        'isoelectric_point': round(analysis.isoelectric_point(), 2),
        'secondary_structure': analysis.secondary_structure_fraction(),  # [helix, turn, sheet]
        'molar_extinction_coefficient': analysis.molar_extinction_coefficient()[0],  # [reduced, oxidized]
        # 'melting_temperature': mt.Tm_NN(prot)  # Calculation based on nearest neighbor thermodynamics
    }

