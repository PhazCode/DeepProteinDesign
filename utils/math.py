import os
import sys
import time
import json
import numpy as np
import pandas as pd
from tqdm import tqdm
from collections import Counter
from sklearn.decomposition import PCA


class EllipsoidSampler:
    """
    samples in a set of points according to their distribution
    """

    def __init__(self, X, n_components=None):
        self.n_components = X.shape[1] if n_components is None else n_components
        pca = PCA(n_components=n_components).fit(X)
        self.components = np.sqrt(pca.explained_variance_) * pca.components_
        self.X_mean = np.mean(X, axis=0)

    def sample(self, n_samples=1, stddev=1):
        M = np.random.randn(n_samples, self.n_components)
        noise = (np.full(n_samples, stddev).reshape((n_samples, 1)) * M).dot(self.components)
        return noise + np.array(n_samples * [self.X_mean])
