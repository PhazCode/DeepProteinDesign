import re
import os


def run(protein):
    """
    Use the EMBOSS program `pepstats` to compute properties of proteins
    used in module protein
    :param p: protein (str)
    :return: dictionnary {seq: {physical_qty_name: value}}
    """
    if 'tmp' not in os.listdir():
        os.system('mkdir tmp')

    with open("tmp/protein.seq", 'w') as f:
        f.write(protein)
    os.system('pepstats -sequence tmp/protein.seq -outfile tmp/protein.pepstat')
    if "protein.pepstat" not in os.listdir('tmp'):
        return None
    with open('tmp/protein.pepstat', 'r') as f:
        content = f.read()
    os.system('rm tmp/protein.seq')
    os.system('rm tmp/protein.pepstat')
    return content


def parse(content):
    properties = dict()
    regex = re.compile(
        r'((Molecular weight *= (\d*\.\d*)) *\t*\n*|(Charge *= (\-*\d*\.\d*)) *\t*\n*|(Isoelectric Point *= (\d*\.\d*))'
        r' *\t*\n*|(A280 Molar Extinction Coefficients *= (\d*\.*\d*)) *\t*\n*|((?:Improbability|Probability) of expression in inclusion bodies *= [-]*(\d*\.*\d*)) *\t*\n*)')
    findings = regex.findall(content)
    for finding in findings:
        if "Molecular" in finding[0]:
            quantity = 'mol_weight'
        elif 'Charge' in finding[0]:
            quantity = 'charge'
        elif 'Isoelec' in finding[0]:
            quantity = 'isoelectric_point'
        elif 'A280' in finding[0]:
            quantity = 'mol_extinction_coef'
        elif 'Probability' in finding[0]:
            quantity = 'proba_expr_inclusion_body'
        elif 'Improbability' in finding[0]:
            quantity = 'improba_expr_inclusion_body'
        value = None
        for element in finding[1:]:
            try:
                value = float(element)
                if quantity == 'improba_expr_inclusion_body':  # @todo: 1 - improba ou improba ?
                    quantity = 'proba_expr_inclusion_body'
                    value = 1 - value
                break
            except Exception:
                continue
        if value is None:
            raise ValueError('parsing wasn\'t well performed because %s was None' % quantity)
        try:
            properties[quantity] = value
        except KeyError:
            properties = {quantity: value}
    if len(properties) != 5:
        raise ValueError
    return properties


def physical_properties(prot):
    return parse(run(prot))
