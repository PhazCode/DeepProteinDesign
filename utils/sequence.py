"""
The module Protein gathers many functions related to proteins
"""
import os
from collections.__init__ import Counter
import subprocess
import numpy as np
from Bio import pairwise2, SeqIO
from collections import Counter
from tqdm import tqdm
from utils import aa_letters, encoding, aa_ids

# basic statistics
# COMPARISION WITH OTHER PROTEINS

B62 = np.load('utils/bio/blosum62.npy')


def distance(seq_a, seq_b, mode='hamming'):
    if mode == 'hamming':
        return sum([a != b for (a, b) in zip(seq_a, seq_b)])
    elif mode == 'align':
        return int(-pairwise2.align.globalms(seq_a, seq_b, 0, -1, -1, -1)[0][2])
    elif mode == 'B62':
        # seq_a is ref seq
        return sum([B62[aa_ids[a], aa_ids[a]] - B62[aa_ids[a], aa_ids[b]] for (a, b) in zip(seq_a, seq_b)])
        # return sum([4 - B62[aa_ids[a], aa_ids[b]] for (a, b) in zip(seq_a, seq_b) if a != b])


def rh(p, r, mode='align'):
    # Reconstruction Homology
    # Same as distance but with 0-1 score
    if mode == 'hamming':
        m = max(len(p), len(r))
        p, r = p + '-' * (m - len(p)), r + '-' * (m - len(r))
        return np.mean([pi == ri for (pi, ri) in zip(p, r)])
    elif mode == 'align':
        s = int(-pairwise2.align.globalms(p, r, 0, -1, -1, -1)[0][2])
        return 1 - s * 2 / (len(p) + len(r))


def align(seq_a, seq_b):
    return pairwise2.align.globalms(seq_a, seq_b, 0, -1, -1, -1)[0][:2]


def ss_scores(seq):
    path = os.path.expanduser('~') + '/PSIPRED/'
    with open(path + 'tmp.fasta', 'w') as f:
        f.write('>tmp\n' + seq)
    subprocess.Popen(['./runpsipred_single', 'tmp.fasta'], cwd=path,
                     stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL).wait()
    table = [x.split(' ') for x in open(path + 'tmp.ss2', 'r').readlines()[2:]]
    t = np.array([[x[-5], x[-3], x[-1][:-1]] for x in table]).astype(np.float32).T
    return t


def ss(seq):
    path = os.path.expanduser('~') + '/PSIPRED/'
    with open(path + 'tmp.fasta', 'w') as f:
        f.write('>tmp\n' + seq)
    subprocess.Popen(['./runpsipred_single', 'tmp.fasta'], cwd=path,
                     stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL).wait()
    with open(path + 'tmp.ss2', 'r') as f:
        res = [s.split(' ')[-8] for s in f.readlines()[2:]]
    return ''.join(res)


def consensus(msa):
    """
    :return: sequence with most frequent amino-acids in eqch position of the msa
    """
    msa_array = np.array([list(seq) for seq in msa]).T
    msa_counts = [Counter(p) for p in msa_array]
    return ''.join([max(c.keys(), key=lambda k: c[k]) for c in msa_counts])


def position_frequencies(msa):
    """
    :param msa:
    :return: matrix M where M[i,a] is frequency of aa a at position i
    """
    n = len(msa)
    msa_array = np.array([list(seq) for seq in msa]).T
    counts = [Counter(pos) for pos in msa_array]
    mat = np.array([[ct[aa] / n for aa in aa_letters] for ct in counts])
    return mat


def align_sequences(seqlist):
    path = os.getcwd() + '/studies/K_ss_viz/'
    encoding.to_fasta(seqlist, path + 'tmp.fa')
    subprocess.Popen(['mafft', 'tmp.fa'], cwd=path,
                     stdout=open(path + 'tmp.txt', 'w'), stderr=subprocess.DEVNULL).wait()
    aligned = [str(r.seq) for r in SeqIO.parse(path + 'tmp.txt', 'fasta')]
    return aligned


def _align_ss(al, ss):
    arr = np.array(list(al))
    arr[arr != '-'] = list(ss)
    return ''.join(arr)


def rearrange(seqlist, distance_function=lambda s1, s2: distance(s1, s2, 'hamming')):
    # GREEDY REARRANGEMENT FOR VIZIBILITY
    n = len(seqlist)
    mat = np.array([[distance_function(s1, s2) for s2 in seqlist] for s1 in seqlist])
    t = list(range(n))
    for i in range(n - 2):
        j = min(range(i + 1, n), key=lambda k: mat[t[k], t[i]])
        t[i + 1], t[j] = t[j], t[i + 1]
    return [seqlist[i] for i in t]


def compute_secondary_structure(seqlist, align=False, remove_gaps=False, reorder=False):
    sslist = [ss(p) for p in tqdm(seqlist)]
    if align:
        seqlist_aligned = align_sequences(seqlist)
        sslist = [_align_ss(al, s) for (al, s) in tqdm(zip(seqlist_aligned, sslist))]
        if remove_gaps:
            sslist = remove_gap_positions(sslist)
    if reorder:
        sslist = rearrange(sslist)
    return sslist


def remove_gap_positions(seqlist, f=0.9, target_size=500):
    n = len(seqlist)
    seqmat = np.array([list(s) for s in seqlist]).T
    gap_frq = np.array([Counter(p)['-'] / n for p in seqmat])
    # keep_idx = np.argpartition(gap_frq, target_size)[:target_size]
    keep_idx = np.where(gap_frq < f)
    return [''.join(l) for l in seqmat[keep_idx].T]
