## Protein Design with Variational Auto Encoders

See [project page](https://gitlab.com/PhazCode/DeepProteinDesign/).

### Quick start

Load model and Luciferase database:
```python
from DB.IPR_LL import db
from generative_models import ProtCNN

model = ProtCNN()
model.load_weights('generative_models/weights/default_ld=100_sstd=1.h5')
```

Train model:
```python
import json
from utils import cond_names
test_proteins = json.load(open('DB/IPR_LL/default_test_proteins.json'))
train_db = db[~db.name.isin(test_proteins)]
model.train(train_db.seq, train_db[cond_names], epochs=50)
```

Make a protein variant:
```python
import numpy as np
from utils import encoding
from utils import luxa_seq as seq
from utils import luxa_conds as conds

x = encoding.to_one_hot([seq])
z = model.encoder.predict(x)
z += np.random.normal(z.shape)
variant = model.reconstruct(z, conds)
```

Equivalent to
```python
variant = model.variant(seq, conds, stddev=1)
```

Compute secondary Structure of a protein (need PSIPRED installed in ~/PSIPRED/) :
```python
from utils import sequence
ss = sequence.ss(seq)
ss_scores = sequence.ss_scores(seq) # scores associated to each structural element
```

Visualize secondary structure of a cluster:
```python
import matplotlib.pyplot as plt
from utils.gui import ss_viz

luxa_cluster_df = db[db.cluster50 == 'P23146']
aligned_seqs = sequence.align_sequences(luxa_cluster_df.seq)
aligned_ss = [sequence._align_ss(seq, ss) for (seq, ss) in
             zip(aligned_seqs, luxa_cluster_df.ss)]
fig, ax = plt.subplots()
ss_viz(ax, aligned_ss)
```
