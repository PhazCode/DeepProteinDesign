# Bokeh Cheat Sheet

## Imports
```python
from bokeh.plotting import figure
from bokeh.io import reset_output, output_notebook, show
output_notebook()
```

## Basics
```python
#create figure
p = figure(title='title', x_range=(0,1), y_range=(0,1),
          x_axis_label='x', y_axis_label='y')

# Regular Plot
p.line(x, y, color='blue', legend = 'legend')

# Scatter Plot
p.circle(x, y, size=7, legend='legend')

# Histogram
hist, edges = np.histogram(x, bins=20)
p.quad(top=hist, bottom=0, left=edges[:-1], right=edges[1:],
        fill_color="blue", line_color="black")

# HeatMap
p.image(image=[matrix[::-1]], x=[0], y=[0], dw=[1], dh=[1], palette="Spectral11")

# ColorBar

# Legend Location
p.legend.location = 'bottom_right'
# Show
show(p)
```

## HeatMap
```python
from bokeh.models import LinearColorMapper, ColorBar
p = figure(title = 'title', x_range=(0, 1), y_range=(0,1))
p.image(image=[matrix[::-1]], x=[0], y=[0], dw=[1], dh=[1], palette="Spectral11")
color_bar = ColorBar(color_mapper=LinearColorMapper(palette = 'Spectral11', low=np.min(m), high=np.max(m)), border_line_color=None, location=(0,0))
p.add_layout(color_bar, 'right')
show(p)
```